import FYBaseUI from "../Tools/FYBaseUI";
import { FYE } from "../Tools/FYE";
import FYUIMgr from "../Tools/FYUIMgr";

const { ccclass, property } = cc._decorator;
/** 消息框 */
@ccclass
export default class UIMessageBox extends FYBaseUI {
    /** 文本框 */
    @property(cc.Label)
    public labContent: cc.Label = null;
    /** 按钮 是 文本 */
    @property(cc.Label)
    public labBtnYes: cc.Label = null;
    /** 按钮 否 文本 */
    @property(cc.Label)
    public labBtnNo: cc.Label = null;
    /** 按钮 确定 文本 */
    @property(cc.Label)
    public labBtnOK: cc.Label = null;
    /** 按钮 是 */
    @property(cc.Node)
    public btnYes: cc.Node = null;
    /** 按钮 否 */
    @property(cc.Node)
    public btnNo: cc.Node = null;
    /** 按钮 确定 */
    @property(cc.Node)
    public btnOK: cc.Node = null;


    private _cbOK = null;
    private _cbYes = null;
    private _cbNo = null;
    private _nameBtnOK = "确定";
    private _nameBtnYes = "是";
    private _nameBtnNo = "否";

    addListener() {
        super.addListener();

        this.btnOK.on(cc.Node.EventType.TOUCH_END, this.onClickBtnOK, this);
        this.btnYes.on(cc.Node.EventType.TOUCH_END, this.onClickBtnYes, this);
        this.btnNo.on(cc.Node.EventType.TOUCH_END, this.onClickBtnNo, this);
    }

    removeListener() {
        super.removeListener();

        this.btnOK.off(cc.Node.EventType.TOUCH_END, this.onClickBtnOK, this);
        this.btnYes.off(cc.Node.EventType.TOUCH_END, this.onClickBtnYes, this);
        this.btnNo.off(cc.Node.EventType.TOUCH_END, this.onClickBtnNo, this);
    }

    // ------------------------- 回调函数 ------------------------- //

    onShow(msgType, param) {
        let type = param[0];
        let content = param[1];
        if (type === FYE.MessageBoxType.OneButton) {
            this._cbOK = param[2];
            this._nameBtnOK = param[3] || this._nameBtnOK;
            this.labBtnOK.string = this._nameBtnOK;
        } else {
            this._cbYes = param[2];
            this._cbNo = param[3];
            this._nameBtnYes = param[4] || this._nameBtnYes;
            this._nameBtnNo = param[5] || this._nameBtnNo;
            this.labBtnYes.string = this._nameBtnYes;
            this.labBtnNo.string = this._nameBtnNo;
        }

        this.btnOK.active = type === FYE.MessageBoxType.OneButton;
        this.btnYes.active = type === FYE.MessageBoxType.TwoButton;
        this.btnNo.active = type === FYE.MessageBoxType.TwoButton;

        this.labContent.string = content;
    }

    onClickBtnOK() {
        if (this._cbOK) {
            this._cbOK();
        }
        FYUIMgr.Instance.close(FYE.UIName.UIMessageBox);
    }

    onClickBtnYes() {
        if (this._cbYes) {
            this._cbYes();
        }
        FYUIMgr.Instance.close(FYE.UIName.UIMessageBox);
    }

    onClickBtnNo() {
        if (this._cbNo) {
            this._cbNo();
        }
        FYUIMgr.Instance.close(FYE.UIName.UIMessageBox);
    }

    // ---------------------- 生命周期 ------------------------------ //


}
