import FYUIMgr from "../Tools/FYUIMgr";
import { FYE } from "../Tools/FYE";
import FYGlobalVarMgr from "../Tools/FYGlobalVarMgr";

/** 消息框管理器 */
export default class MessageBoxMgr {
    public static readonly Instance: MessageBoxMgr = new MessageBoxMgr();
    private constructor() { }

    /**
     * 一个按钮消息
     * @param content 消息内容
     * @param cbOK 确定回调
     * @param btnName 确定按钮文本
     */
    public showOneBtn(content, cbOK = null, btnName = "") {
        FYUIMgr.Instance.show(FYE.UIName.UIMessageBox, FYGlobalVarMgr.Instance.NodeTop, FYE.MessageBoxType.OneButton, content, cbOK, btnName);
    }

    /**
     * 两个按钮消息
     * @param content 消息内容
     * @param cbYes 确定回调
     * @param cbNo 取消回调
     * @param btnYesName 确定文本内容
     * @param btnNoName 取消文本内容
     */
    public showTwoBtn(content, cbYes = null, cbNo = null, btnYesName = "", btnNoName = "") {
        FYUIMgr.Instance.show(FYE.UIName.UIMessageBox, FYGlobalVarMgr.Instance.NodeTop, FYE.MessageBoxType.TwoButton, content, cbYes, cbNo, btnYesName, btnNoName);
    }
}
