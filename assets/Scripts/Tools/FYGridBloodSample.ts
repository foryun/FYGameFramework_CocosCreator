import FYBaseNode from "./FYBaseNode";
import FYScheduleMgr from "./FYScheduleMgr";
import FYMessenger from "./FYMessenger";
import { FYE } from "./FYE";


const { ccclass, property } = cc._decorator;
/** 
 * 单格血量对象
 * 用法：
 *      由FYGridBlood控制，用户不需要主动调用。
 */
@ccclass
export default class FYGridBloodSample extends FYBaseNode {
    /** 血量恢复 */
    @property(cc.Sprite)
    public bloodRestore: cc.Sprite = null;
    /** 血量恢复满 */
    @property(cc.Node)
    public bloodFull: cc.Node = null;
    /** 回血比例 */
    private _bloodRestoreRadio: number = 0;
    /** 血量恢复所需时间 单位毫秒 */
    private _bloodRestoreTime: number = 6000;
    /** 血量恢复动画刷新频率 单位毫秒 */
    private _refreshTime: number = 300;
    /** 恢复一格血回调 */
    private _cbRestoreBlood = null;

    /** 恢复一格血回调 */
    public init(cb) {
        this._cbRestoreBlood = cb;
        this.setBloodRestoreRadio(this._bloodRestoreTime / this._refreshTime);
    }

    /** 获取血量恢复比例 */
    public getBloodRestoreRadio() {
        return this._bloodRestoreRadio;
    }

    /** 是否正在恢复 */
    public isRestoring() {
        if (this._bloodRestoreRadio > 0 && this._bloodRestoreRadio < this._bloodRestoreTime / this._refreshTime) {
            return true;
        } else {
            return false;
        }
    }

    public setBloodRestoreRadio(radio: number) {
        this._bloodRestoreRadio = radio;
        this.bloodRestore.fillRange = this._bloodRestoreRadio * this._refreshTime / this._bloodRestoreTime
        FYScheduleMgr.Instance.unscheduleAllCallbacks(this.node);
        this.bloodFull.active = this._bloodRestoreRadio === this._bloodRestoreTime / this._refreshTime;
    }

    /** 开始 */
    public startRestoreBlood() {
        if (this.bloodFull.active) {
            return;
        }

        this.bloodRestore.fillRange = 0;
        let self = this;
        let interval = this._refreshTime / 1000;
        let repeat = this._bloodRestoreTime / this._refreshTime - this._bloodRestoreRadio;

        FYScheduleMgr.Instance.schedule(function () {
            self.bloodRestore.fillRange = self._bloodRestoreRadio * self._refreshTime / self._bloodRestoreTime;
            self._bloodRestoreRadio++;
            if (self._bloodRestoreRadio === self._bloodRestoreTime / self._refreshTime) {
                self.bloodFull.active = true;
                if (self._cbRestoreBlood) {
                    self._cbRestoreBlood();
                }
            }
        }.bind(this), this.node, interval, repeat, 0);
    }

    /** 扣血 */
    public subBlood() {
        this.bloodFull.active = false;
        this.bloodRestore.fillRange = 0;
        this._bloodRestoreRadio = 0;
        this.startRestoreBlood();
    }

    // ---------------------------- 生命周期 ------------------------------ //

    onEnable() {
        super.onEnable();

        this._bloodRestoreRadio = this._bloodRestoreTime / this._refreshTime
    }

    onDisable() {
        super.onDisable();

        FYScheduleMgr.Instance.unscheduleAllCallbacks(this.node);
        this.bloodRestore.fillRange = 1;
        this.bloodFull.active = true;
    }
}
